// Written by Oleh Fedkevych
// oleh.fedkevych@ge.infn.it 
#include "Rivet/Analysis.hh"

#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/ZFinder.hh"
#include "Rivet/Projections/FastJets.hh"

#include "Rivet/Tools/RivetHepMC.hh"


/// \class HEP2_writer
/// a simple helper class to output HepMC2 events to a file
// Basically, a standard way to output HepMC files is as follows:
/*
// Specify file where HepMC events will be stored.

HepMC::IO_GenEvent ascii_io("output.hepmc", std::ios::out);

// Construct new empty HepMC event.
const HepMC::GenEvent *hepmcevt = event.genEvent();
        
ascii_io << hepmcevt;
*/
//---------------------------------------------------------------------------------------------------------------------
class HEP2_writer
{
public:
        HEP2_writer():  _ascii_io("output.hepmc", std::ios::out) {}
        
        void pass_event(const HepMC::GenEvent *hepPtr) {_ascii_io << hepPtr;}
        
        ~HEP2_writer() {_ascii_io.clear();}

private:
        HepMC::IO_GenEvent _ascii_io;
};

namespace Rivet 
{
class HEP2_OUTPUT : public Analysis,  HEP2_writer
{
public:
    HEP2_OUTPUT() : Analysis("HEP2_OUTPUT") {} 
    
    // Jet Radius
    const double _jetR = 0.4;
    
    void init () 
    {
        // Let's define some projections to have a simple analysis
        // Initialise and register projections
        FinalState fs(-5, 5, 0.0*GeV);
    
        // for the muons
        double mu_pt = 26.;
        double mz_min = (90-20);
        double mz_max = (90+20);
        double eta_max = 2.4;
    
        ZFinder zfinder(fs,
                    Cuts::pT > mu_pt*GeV  && Cuts::abseta < eta_max,
                    PID::MUON,
                    mz_min*GeV, mz_max*GeV,
                    0.1, ZFinder::NOCLUSTER, ZFinder::NOTRACK);
        addProjection(zfinder, "ZFinder");

        // Particles for the jets
        VetoedFinalState jet_input(fs);
        jet_input.vetoNeutrinos();
        jet_input.addVetoOnThisFinalState(getProjection<ZFinder>("ZFinder"));
        addProjection(jet_input, "JET_INPUT");
        
        // FastJet projections
        declare("AKTJETS", FastJets(jet_input, FastJets::ANTIKT, _jetR));
    }
        
    void analyze(const Event &event) 
    {
        // Let's run our analysis on a given event
        // Reconstruct Z-boson
        const ZFinder &zfinder = applyProjection<ZFinder>(event, "ZFinder");
            
        if (zfinder.bosons().size() < 1) vetoEvent;

        // Now do selection criteria
        // Do b-tagging tagging
        const Jets jets = apply<FastJets>(event, "AKTJETS").jetsByPt(Cuts::pT > 15*GeV);
      
        Jets btag_jets =  filter_select(jets, hasBTag(Cuts::pT > 5*GeV));
 
        if (btag_jets.size() < 1) vetoEvent;
        
        // Print event into file
        pass_event(event.genEvent());
    }
            
    void finalize() {}
};
    
    DECLARE_RIVET_PLUGIN(HEP2_OUTPUT);
}

